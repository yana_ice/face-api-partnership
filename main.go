package main

import (
	"log"

	"gitlab.com/yana_ice/face-api-partnership/handler"
)

func main() {
	log.Print("Start service")
	handler.Init()
}
