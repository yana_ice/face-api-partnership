package service

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func FaceDetect(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
func FaceQuality(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
func FaceCompare(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
func FaceImageCompare(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
func FaceIdentification(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
