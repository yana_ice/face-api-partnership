package service

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// FaceSetFaceAdd ...
func FaceSetFaceAdd(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}

// FaceSetFaceDelete ...
func FaceSetFaceDelete(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}

// FaceSetFaceList ...
func FaceSetFaceList(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
