package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/yana_ice/face-api-partnership/service"
)

// Init ...
func Init() {
	r := gin.Default()

	r.GET("/system/health", healthCheck)

	face := r.Group("/face")
	{
		face.POST("/detect", service.FaceDetect)
		face.POST("/quality", service.FaceQuality)
		face.POST("/faceid-compare", service.FaceCompare)
		face.POST("/image-compare", service.FaceImageCompare)
		face.POST("/identification", service.FaceIdentification)
	}

	faceSet := r.Group("/face-set")
	{
		faceSet.POST("/create", service.FaceSetCreate)
		faceSet.POST("/delete", service.FaceSetDelete)
		faceSet.POST("/update", service.FaceSetUpdate)
		faceSet.POST("/get", service.FaceSetGet)
		faceSet.POST("/list", service.FaceSetList)
	}

	faceSetFace := r.Group("face-set-face")
	{
		faceSetFace.POST("/add", service.FaceSetFaceAdd)
		faceSetFace.POST("/delete", service.FaceSetFaceDelete)
		faceSetFace.POST("/list", service.FaceSetFaceList)
	}

	r.Run(":8080")
}

func healthCheck(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}
